using System.Globalization;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.Extensions.Options;
using Mollie.Api.Client;
using Mollie.Api.Client.Abstract;
using Mollie.Api.Models;
using Mollie.Api.Models.Payment.Request;
using Mollie.Api.Models.Payment.Response;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddValidatorsFromAssemblyContaining<PaymentRequestViewModel>(lifetime: ServiceLifetime.Scoped);
var mollieSettingsSection = builder.Configuration.GetSection(nameof(MollieSettings));
builder.Services.Configure<MollieSettings>(mollieSettingsSection);

// Add services to the container.
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.MapPost("/payment/create", async (
    IOptions<MollieSettings> _mollieSettings,
    IValidator<PaymentRequestViewModel> validator, 
    PaymentRequestViewModel paymentRequestViewModel) =>
{
    IPaymentClient paymentClient = new PaymentClient(_mollieSettings.Value.ApiKey);

    ValidationResult validationResult = validator.Validate(paymentRequestViewModel);

    if (!validationResult.IsValid)
    {
        return Results.ValidationProblem(validationResult.ToDictionary());
    }

    try
    {
        var paymentRequest = new PaymentRequest();
        paymentRequest.Amount = new Amount(
            paymentRequestViewModel.Currency, 
            paymentRequestViewModel.Amount.ToString(CultureInfo.InvariantCulture)
        );
        // paymentRequest.ApplicationFee etc

        PaymentResponse result = await paymentClient.CreatePaymentAsync(paymentRequest);
        return Results.Ok(result.CreatedAt);
    }
    catch(Exception ex)
    {
        return Results.Problem("error processing payment." + " " + ex.Message);
    }
})
.WithName("CreatePayment")
.Produces(200)
.Produces(400);

app.Run();

public record MollieSettings
{
    public string ApiKey {get; set; } = string.Empty;
}

public record PaymentRequestViewModel(decimal Amount, string Currency)
{
    public class Validator : AbstractValidator<PaymentRequestViewModel>
    {
        public Validator()
        {
            RuleFor(x => x.Amount)
                .NotNull().WithMessage("amount_required")
                .GreaterThan(0).WithMessage("amount to low :(")
                .ScalePrecision(2, 5).WithMessage("precision");
            RuleFor(x => x.Currency).Length(3).WithMessage("currency_invalid");
        }
    }
}

public static class ValidationExtensions
{
    public static IDictionary<string, string[]> ToDictionary(this ValidationResult validationResult)
        => validationResult.Errors
                .GroupBy(x => x.PropertyName)
                .ToDictionary(
                    g => g.Key,
                    g => g.Select(x => x.ErrorMessage).ToArray()
                );
}

